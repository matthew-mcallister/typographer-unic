use self::Side::*;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
crate enum Side { Close, Open, Clopen }

impl Side {
    fn from_open(is_open: bool) -> Self {
        if is_open { Open } else { Close }
    }

    #[cfg(test)]
    fn opp(self) -> Self {
        match self {
            Open => Close,
            Close => Open,
            _ => self,
        }
    }
}

macro_rules! impl_brackets {
    ($(($left:expr, $right:expr))*) => {
        const BRACKETS: &'static [(u16, u16)] = &[$(($left, $right),)*];
    }
}

include!(concat!(env!("OUT_DIR"), "/impl_brackets.rs"));

const BRACKETS_MIN: u32 = BRACKETS[0].0 as u32;
const BRACKETS_MAX: u32 = BRACKETS[BRACKETS.len() - 1].0 as u32;

fn bracket_lookup(ch: u16) -> Option<(u32, Side)> {
    let i = BRACKETS.binary_search_by_key(&ch, |&(c, _)| c).ok()?;
    Some((
        BRACKETS[i].1 as u32,
        Side::from_open(i % 2 == 0),
    ))
}

const MAX_MATCHES: usize = 3;
crate type Matches = [u32; MAX_MATCHES];

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
crate struct BracketInfo {
    matches: Matches,
    side: Side,
}

macro_rules! bracket {
    ($ch:expr) => { [$ch as _; MAX_MATCHES] };
    ($ch1:expr, $ch2:expr) => { [$ch1 as _, $ch2 as _, $ch2 as _] };
    ($ch1:expr, $ch2:expr, $ch3:expr) => {
        [$ch1 as _, $ch2 as _, $ch3 as _]
    };
}

const fn close(matches: Matches) -> BracketInfo
    { BracketInfo { side: Close, matches } }
const fn open(matches: Matches) -> BracketInfo
    { BracketInfo { side: Open, matches } }
const fn clopen(matches: Matches) -> BracketInfo
    { BracketInfo { side: Clopen, matches } }

const QUOTES_MIN: u32 = 0x2018;

// ATM, if the bracket is clopen, only gives the matches for when it is
// used as a closing bracket.
fn bracket_info(ch: char) -> Option<BracketInfo> {
    match ch as u32 {
        0..256 => Some(match ch {
            '"' => clopen(bracket!('"')),
            '(' => open(bracket!(')')),
            ')' => close(bracket!('(')),
            '[' => open(bracket!(']')),
            ']' => close(bracket!('[')),
            '{' => open(bracket!('}')),
            '}' => close(bracket!('{')),
            // »...«
            '\u{ab}' => clopen(bracket!('\u{bb}')),
            // «...» »...»
            '\u{bb}' => clopen(bracket!('\u{ab}', '\u{bb}')),
            _ => return None,
        }),
        QUOTES_MIN..BRACKETS_MIN => Some(match ch {
            // ’...‘ ‚...‘
            '\u{2018}' => clopen(bracket!('\u{2019}', '\u{201a}')),
            // ‘...’ ’...’ ‚...’
            '\u{2019}' => clopen(bracket!('\u{2018}', '\u{2019}', '\u{201a}')),
            '\u{201a}' => open(bracket!('\u{2018}', '\u{2019}')),
            // ”...“ „...“
            '\u{201c}' => clopen(bracket!('\u{201d}', '\u{201e}')),
            // “...” ”...” „...”
            '\u{201d}' => clopen(bracket!('\u{201c}', '\u{201d}', '\u{201e}')),
            '\u{201e}' => open(bracket!('\u{201c}', '\u{201d}')),
            // ‹...›
            '\u{2039}' => clopen(bracket!('\u{203a}')),
            // ›...‹ ›...›
            '\u{203a}' => clopen(bracket!('\u{2039}', '\u{203a}')),
            _ => return None,
        }),
        BRACKETS_MIN...BRACKETS_MAX => {
            let ch = ch as u16;
            let (opp, side) = bracket_lookup(ch)?;
            Some(BracketInfo { side, matches: bracket!(opp) })
        },
        _ => None,
    }
}

impl BracketInfo {
    crate fn from_char(ch: char) -> Option<Self> { bracket_info(ch) }

    crate fn side(&self) -> Side { self.side }

    // Tells if a closing bracket matches a given opening bracket.
    crate fn matches(&self, ch: char) -> bool {
        self.matches.iter().any(|&c| c == ch as u32)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bracket_info() {
        const PAIRS: &'static [(char, Option<BracketInfo>)] = &[
            ('\u{0f3a}', None),
            ('\u{207d}', Some(open(bracket!('\u{207e}')))),
            ('\u{207e}', Some(close(bracket!('\u{207d}')))),
        ];
        for &(ch, info) in PAIRS.iter() {
            assert_eq!(BracketInfo::from_char(ch), info);
        }
    }

    #[test]
    fn test_bracket_symmetry() {
        for &(ch1, ch2) in BRACKETS.iter() {
            let (ch1, ch2) = unsafe { (
                std::char::from_u32_unchecked(ch1 as u32),
                std::char::from_u32_unchecked(ch2 as u32),
            ) };

            let info1 = bracket_info(ch1).unwrap();
            let info2 = bracket_info(ch2).unwrap();
            assert_eq!(info1.side, info2.side.opp());

            let a = info1.matches(ch2);
            let b = info2.matches(ch1);
            assert!(a || b);
        }
    }
}
