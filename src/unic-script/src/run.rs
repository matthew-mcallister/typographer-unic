//! This module implements the `ScriptIter` type, which finds breaks up
//! text into homogenous runs based on script for use by graphical
//! applications.
use crate::*;
use crate::bracket::*;

/// A stretch of text written in a uniform script. This is returned by
/// iterating over the `ScriptIter` type.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Run {
    /// Length in bytes.
    pub len: usize,
    pub script: Script,
}

#[derive(Clone, Copy, Debug)]
struct StackEntry {
    pos: usize,
    ch: char,
    script: Script,
}

#[derive(Clone, Copy, Debug)]
struct CharInfo {
    pos: usize,
    ch: char,
    sc: Script,
    scx: Option<&'static [Script]>,
}

/// An iterator over the same-script runs in a piece of text.
///
/// # Examples
///
/// ```
/// use unic_script::{*, run::*};
/// let input = "Hello, 世界!";
/// let runs: Vec<_> = ScriptIter::new(input).collect();
/// assert_eq!(&runs[..], &[
///     Run { len: 7, script: Script::Latin },
///     Run { len: 7, script: Script::Han },
/// ]);
/// ```
///
/// When the iterator cannot readily determine the script code of a
/// character from context, it eagerly aborts the run when convenient
/// and returns the `Unknown` script code. It may even do this multiple
/// times consecutively.
/// ```
/// use unic_script::{*, run::*};
/// let input = "ー.ーー.";
/// let runs: Vec<_> = ScriptIter::new(input).collect();
/// assert_eq!(&runs[..], &[
///     Run { len: 3, script: Script::Unknown },
///     Run { len: 7, script: Script::Unknown },
///     Run { len: 1, script: Script::Unknown },
/// ]);
/// ```
#[derive(Clone, Debug)]
pub struct ScriptIter<'src> {
    source: &'src str,
    chars: std::str::CharIndices<'src>,
    // TODO: The bidi spec says the stack length should be limited to 63
    // Otherwise, the algorithm is O(n^2)!
    stack: Vec<StackEntry>,
    run_sc: Script,
    cur_ch: Option<CharInfo>,
}

impl<'src> ScriptIter<'src> {
    /// Constructs a new iterator.
    pub fn new(source: &'src str) -> Self {
        let mut iter = ScriptIter {
            source,
            chars: source.char_indices(),
            stack: Vec::new(),
            run_sc: Script::Unknown,
            cur_ch: None,
        };
        // Get the first char
        advance(&mut iter);
        iter
    }
}

impl<'src> Iterator for ScriptIter<'src> {
    type Item = Run;
    fn next(&mut self) -> Option<Self::Item> { next_run(self) }
}

impl<'src> std::iter::FusedIterator for ScriptIter<'src> {}

fn ambig(sc: Script) -> bool {
    sc == Script::Common || sc == Script::Inherited
}

fn stack_push(iter: &mut ScriptIter<'_>, pos: usize, ch: char) {
    // Don't inherit the `Unknown` script
    let script =
        if iter.run_sc == Script::Unknown { Script::Common }
        else { iter.run_sc };
    iter.stack.push(StackEntry { pos, ch, script });
}

fn advance(iter: &mut ScriptIter<'_>) -> Option<CharInfo> {
    iter.cur_ch = advance_inner(iter);
    iter.cur_ch
}

fn advance_inner(iter: &mut ScriptIter<'_>) -> Option<CharInfo> {
    let (pos, ch) = iter.chars.next()?;

    let mut sc = Script::from_char(ch);

    if sc == Script::Common {
        if let Some(info) = BracketInfo::from_char(ch) {
            match info.side() {
                Side::Open => { stack_push(iter, pos, ch); },
                Side::Close => {
                    // Pop matching entry off the stack (if one exists)
                    // as well as any unmatched entries above it.
                    if let Some(idx) = iter.stack.iter()
                        .rposition(|entry| info.matches(entry.ch))
                    {
                        sc = iter.stack[idx].script;
                        iter.stack.truncate(idx);
                    }
                },
                Side::Clopen => {
                    // Only pop the topmost entry (if it matches). This
                    // is so we don't screw up alternating quote marks.
                    match iter.stack.last() {
                        Some(&entry) if info.matches(entry.ch) => {
                            sc = entry.script;
                            iter.stack.pop().unwrap();
                        },
                        _ => { stack_push(iter, pos, ch); }
                    }
                },
            }
        }
    }

    let scx = if ambig(sc) { Script::raw_exts_from_char(ch) } else { None };

    Some(CharInfo { pos, ch, sc, scx })
}

fn next_run(iter: &mut ScriptIter<'_>) -> Option<Run> {
    // N.B.: This algorithm is modified from Pango's implementation and
    // incorporates the advice to implementors found in UAX #24.
    let start = iter.cur_ch?.pos;

    // Skip ahead until we find a char that tells us the script
    iter.run_sc = Script::Unknown;
    while let Some(ch) = iter.cur_ch {
        if !ambig(ch.sc) || ch.scx.is_some() { break; }
        advance(iter);
    }

    // Punt if unable to determine the script at this point
    if let Some(ch) = iter.cur_ch {
        if !ambig(ch.sc) { iter.run_sc = ch.sc; }
    }

    // Retroactively apply script information to the bracket stack
    for entry in iter.stack.iter_mut().rev()
        .take_while(|entry| entry.pos >= start)
    {
        entry.script = iter.run_sc;
    }

    if iter.run_sc == Script::Unknown {
        // Try to end the run ASAP
        while let Some(ch) = advance(iter) {
            if ch.sc != Script::Unknown && ch.scx.is_none() { break; }
        }
    } else {
        while let Some(ch) = advance(iter) {
            if ambig(ch.sc) {
                if let Some(scx) = ch.scx {
                    if !scx.contains(&iter.run_sc) { break; }
                }
            } else if ch.sc != iter.run_sc { break; }
        }
    }

    let len =
        if let Some(ch) = iter.cur_ch { ch.pos - start }
        else { iter.source.len() - start };
    if len == 0 { return None; }

    let run = Run { len, script: iter.run_sc };

    Some(run)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_script_iter() {
        const PAIRS: &'static [(&'static str, &'static [Run])] = &[
            ("Hello, world!", &[Run { len: 13, script: Script::Latin }]),
            (r"\.(o.o)./", &[Run { len: 9, script: Script::Latin }]),
            ("[a]α", &[
                Run { len: 3, script: Script::Latin },
                Run { len: 2, script: Script::Greek },
            ]),
            ("aー", &[
                Run { len: 1, script: Script::Latin },
                Run { len: 3, script: Script::Unknown },
            ]),
            ("ー()a", &[
                Run { len: 3, script: Script::Unknown },
                Run { len: 3, script: Script::Latin },
            ]),
            ("()a", &[Run { len: 3, script: Script::Latin }]),
            ("a({[⟨א⟩]})ナ", &[
                Run { len: 7, script: Script::Latin },
                Run { len: 2, script: Script::Hebrew },
                Run { len: 6, script: Script::Latin },
                Run { len: 3, script: Script::Katakana },
            ]),
            ("\u{0300}o\u{0301}", &[
                Run { len: 5, script: Script::Latin },
            ]),
            // The unmatched } will not clobber the stack
            ("α(}c)", &[
                Run { len: 4, script: Script::Greek },
                Run { len: 1, script: Script::Latin },
                Run { len: 1, script: Script::Greek },
            ]),
            // The matched } *will* clobber the stack
            ("{α(}c)", &[
                Run { len: 5, script: Script::Greek },
                Run { len: 2, script: Script::Latin },
            ]),
            // Quotation marks are matched
            (r#"a"人""#, &[
                Run { len: 2, script: Script::Latin },
                Run { len: 3, script: Script::Han },
                Run { len: 1, script: Script::Latin },
            ]),
            (r#"a"人«c»""#, &[
                Run { len: 2, script: Script::Latin },
                Run { len: 5, script: Script::Han },
                Run { len: 1, script: Script::Latin },
                Run { len: 2, script: Script::Han },
                Run { len: 1, script: Script::Latin },
            ]),
            // Strange nestings are allowed as long as the quotes
            // alternate properly
            (r#"a»人›a⟨«ψ»⟩‹»"#, &[
                Run { len: 3, script: Script::Latin },
                Run { len: 6, script: Script::Han },
                Run { len: 6, script: Script::Latin },
                Run { len: 2, script: Script::Greek },
                Run { len: 5, script: Script::Latin },
                Run { len: 3, script: Script::Han },
                Run { len: 2, script: Script::Latin },
            ]),
            // Apostrophes as single quotes are impossible without
            // specialized language support.
            (r#"一"'"a"'""#, &[
                Run { len: 6, script: Script::Han },
                Run { len: 4, script: Script::Latin },
            ]),
        ];
        for (input, expected) in PAIRS.iter() {
            // It's easy to misapprehend the length of utf-8 strings
            assert_eq!(input.len(), expected.iter().map(|run| run.len).sum());

            let runs: Vec<_> = ScriptIter::new(input).collect();
            assert_eq!(&runs, expected, "\n input: {:?}", input);
        }
    }
}
