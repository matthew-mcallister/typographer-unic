use std::env;
use std::process::Command;

fn main() {
    println!("cargo:rerun-if-changed=bin/codegen");
    println!("cargo:rerun-if-changed=data/PropertyValueAliases.txt");
    println!("cargo:rerun-if-changed=data/Scripts.txt");
    println!("cargo:rerun-if-changed=data/ScriptExtensions.txt");

    let out_dir = env::var_os("OUT_DIR").unwrap();
    let status = Command::new("bin/codegen")
        .args(&["-i", "data"])
        .arg("-o").arg(out_dir)
        .status().unwrap();
    assert!(status.success());
}
