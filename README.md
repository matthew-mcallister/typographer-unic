# typographer-unic

This repository houses implementations of several Unicode algorithms for
use by text layout software for graphical applications---namely the
`typographer` engine (as the name implies). The name is partly derives
from the [`unic`][unic] project, as work on this repository may one day
make its way over there.

[unic]: https://github.com/open-i18n/rust-unic
